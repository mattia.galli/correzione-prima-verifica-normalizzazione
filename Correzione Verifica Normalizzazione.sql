drop database if exists db_correzione_normalizzazione;
create database db_correzione_normalizzazione;
use db_correzione_normalizzazione;

create table dati(
cod_ordine varchar(10),
data_ordine datetime,
cod_prod varchar(10),
descrizione_prod varchar(10),
prezzo_vendita_prod decimal(14,4),
quantita_prod int,
prezzo_unitario_prod decimal(14,4),
giacenza_prod int,
cod_cliente varchar(10),
nominativo varchar(10)
);

alter table dati add constraint pk_cod_ordine_cod_prodotto primary key(cod_ordine, cod_prod);

insert into dati values ('OR01', '2021-1-1T09:00', 'P01', 'PC xx', 1150.00, 1, 1200.00, 9, 'C01', 'Rossi');
insert into dati values ('OR01', '2021-1-1T09:00', 'P02', 'Stampante', 300.00, 1, 300.00, 19, 'C01', 'Rossi');
insert into dati values ('OR02', '2021-2-2T10:00', 'P01', 'PC xx', 1100.00, 2, 1200.00, 7, 'C02', 'Verdi');
insert into dati values ('OR02', '2021-2-2T10:00', 'P03', 'Tablet YY', 350.00, 1, 350.00, 3, 'C02', 'Verdi');
insert into dati values ('OR03', '2021-2-2T14:00', 'P03', 'Tablet YY', 340.00, 1, 350.00, 2, 'C01', 'Rossi');
insert into dati values ('OR04', '2021-2-3T11:00', 'P01', 'PC xx', 1150.00, 1, 1200.00, 5, 'C02', 'Verdi');

create table clienti(
cod_cliente varchar(10),
nominativo varchar(10)
);

create table prodotti(
cod_prod varchar(10),
descrizione_prod varchar(10),
prezzo_unitario_prod decimal(14,4),
giacenza_prod int
);

create table ordini(
cod_ordine varchar(10),
data_ordine datetime,
cod_cliente varchar(10)
);

create table dettaglio_ordini(
cod_ordine varchar(10),
cod_prod varchar(10),
prezzo_vendita_prod decimal(14,4),
quantita_prod int
);

alter table clienti add constraint pk_clienti_cod_cliente primary key (cod_cliente);
alter table prodotti add constraint pk_prodotti_cod_prodotto primary key (cod_prod);
alter table ordini add constraint pk_ordini_cod_ordine primary key (cod_ordine);
alter table ordini add constraint fk_ordini_cod_cliente foreign key (cod_cliente) references clienti(cod_cliente);
alter table dettaglio_ordini add constraint pk_dettaglio_ordini_cod_ordine_cod_prod primary key (cod_ordine, cod_prod);
alter table dettaglio_ordini add constraint fk_dettaglio_ordini_cod_cliente foreign key (cod_ordine) references ordini(cod_ordine);
alter table dettaglio_ordini add constraint fk_dettaglio_ordini_cod_prod foreign key (cod_prod) references prodotti(cod_prod);


insert into clienti select distinct cod_cliente, nominativo from dati;
insert into prodotti select distinct cod_prod, descrizione_prod, prezzo_unitario_prod, min(giacenza_prod) from dati group by cod_prod;
insert into ordini select distinct cod_ordine, data_ordine, cod_cliente from dati;
insert into dettaglio_ordini select distinct cod_ordine, cod_prod, prezzo_vendita_prod, quantita_prod from dati;

delimiter $$
create trigger update_giacenza
after insert on dettaglio_ordini
for each row
begin
	update prodotti set prodotti.giacenza_prod = (prodotti.giacenza_prod - new.quantita_prod)
    where prodotti.cod_prod = new.cod_prod;
end$$
delimiter ;

-- Esercizio 3: scrivere un’istruzione sql che consenta di calcolare l’importo totale per ogni ordine: cod_ordine,data,importo;
	select d.cod_ordine, data_ordine, sum(prezzo_vendita_prod * quantita_prod) as importo
    from dettaglio_ordini d inner join ordini o on d.cod_ordine = o.cod_ordine 
    group by cod_ordine;

-- Esercizio 4: scrivere un’istruzione sql che consenta di calcolare l’importo totale per ogni cliente: nominativo, importo;

	drop view if exists dettaglio_ordini_ordini;
    create view dettaglio_ordini_ordini as
    select o.cod_ordine, cod_cliente, prezzo_vendita_prod, quantita_prod 
    from ordini o inner join dettaglio_ordini d on o.cod_ordine = d.cod_ordine;

	select nominativo, sum(prezzo_vendita_prod * quantita_prod) as importo
    from dettaglio_ordini_ordini d inner join clienti o on d.cod_cliente = o.cod_cliente
    group by nominativo;
	
-- Esercizio 5: scrivere un’istruzione sql che consenta di calcolare l’importo totale per ogni prodotto: cod_prodotto,descrizione,importo;
	select d.cod_prod, descrizione_prod, sum(prezzo_vendita_prod * quantita_prod) as importo
	from dettaglio_ordini d inner join prodotti p on d.cod_prod = p.cod_prod
	group by cod_prod;

-- Esercizio 6: scrivere un’istruzione sql che consenta di trovare il prodotto pìù venduto: cod_prodotto,descrizione;
	drop view if exists cod_prod_quantita;
    create view cod_prod_quantita as 
    select cod_prod, sum(quantita_prod) as quantita from dettaglio_ordini group by cod_prod;
    
	select p.cod_prod, descrizione_prod
	from prodotti p inner join (select cod_prod, max(quantita) from cod_prod_quantita)  as c on p.cod_prod = c.cod_prod;
    
-- Esercizio 7: scrivere un’istruzione sql che consenta di trovare il cliente con il fatturato maggiore: nominativo,importo;
	drop view if exists cliente_importo;
    create view cliente_importo as 
    select cod_cliente, sum(prezzo_vendita_prod * quantita_prod) as importo 
        from dettaglio_ordini d inner join ordini o on d.cod_ordine = o.cod_ordine
        group by cod_cliente;
        
	drop view if exists cliente_importo_max;
    create view cliente_importo_max as
    select *
	from cliente_importo
    where importo = (select max(importo) from cliente_importo);

	select nominativo, importo
    from cliente_importo_max cim inner join clienti c on cim.cod_cliente = c.cod_cliente;
    
-- Esercizio 8: scrivere un’istruzione sql che consenta di trovare il giorno con la frequenza di vendite maggiore: data, frequenza;
	drop view if exists cod_data;
    create view cod_data as
    select o.cod_ordine, data_ordine
    from ordini o inner join dettaglio_ordini d on o.cod_ordine = d.cod_ordine;
    
    drop view if exists data_frequenza;
    create view data_frequenza as
	select data_ordine, count(*) as frequenza
	from cod_data
	group by data_ordine;
    
    select data_ordine, max(frequenza)
    from data_frequenza;
